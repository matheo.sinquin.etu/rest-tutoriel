package fr.ulille.iut.tva.dto;

public class Detaildto {

	private double montantTotal;
	private double montantTVA;
	private double somme;
	private String tauxLabel;
	private double tauxValue;
	
	public Detaildto(double montantTotal, double montantTVA, double somme, String tauxLabel, double tauxValue) {
		this.montantTotal = montantTotal;
		this.montantTVA = montantTVA;
		this.somme = somme;
		this.tauxLabel = tauxLabel;
		this.tauxValue = tauxValue;
	}
	
	public double getMontantTotal() {
		return montantTotal;
	}
	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}
	public double getMontantTVA() {
		return montantTVA;
	}
	public void setMontantTVA(double montantTVA) {
		this.montantTVA = montantTVA;
	}
	public double getSomme() {
		return somme;
	}
	public void setSomme(double somme) {
		this.somme = somme;
	}
	public String getTauxLabel() {
		return tauxLabel;
	}
	public void setTauxLabel(String tauxLabel) {
		this.tauxLabel = tauxLabel;
	}
	public double getTauxValue() {
		return tauxValue;
	}
	public void setTauxValue(double tauxValue) {
		this.tauxValue = tauxValue;
	}

}
